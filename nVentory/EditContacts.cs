﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using nVentory.Actions;

namespace nVentory
{
    public partial class EditContacts : Form
    {
        nVentoryEntities nVentoryDB = new nVentoryEntities();
        public EditContacts()
        {
            InitializeComponent();
            LoadStartupItems();

        }

        private void LoadStartupItems()
        {
            foreach (var contactType in nVentoryDB.ContactTypes)
            {
                combobx_CustomerType.Items.Add(contactType.TypeName);
            }
            if (combobx_CustomerType.Items.Count >= 1)
            {
                combobx_CustomerType.SelectedItem = combobx_CustomerType.Items[0];
            }
        }

        private void InitializeComponent()
        {
            this.btn_Save = new System.Windows.Forms.Button();
            this.txtbx_ContactDescription = new System.Windows.Forms.TextBox();
            this.lbl_ProductDescription = new System.Windows.Forms.Label();
            this.txtbx_ContactName = new System.Windows.Forms.TextBox();
            this.lbl_ProductName = new System.Windows.Forms.Label();
            this.combobx_CustomerType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtbx_ContactCity = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbx_ContactAddress = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtbx_ContactZipCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtbx_ContactState = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbx_ContactPhoneNumber = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtbx_ContactEmailAddress = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtbx_ContactNotes = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(308, 302);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(75, 23);
            this.btn_Save.TabIndex = 9;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.SaveContact);
            // 
            // txtbx_ContactDescription
            // 
            this.txtbx_ContactDescription.Location = new System.Drawing.Point(126, 56);
            this.txtbx_ContactDescription.Name = "txtbx_ContactDescription";
            this.txtbx_ContactDescription.Size = new System.Drawing.Size(257, 20);
            this.txtbx_ContactDescription.TabIndex = 8;
            // 
            // lbl_ProductDescription
            // 
            this.lbl_ProductDescription.AutoSize = true;
            this.lbl_ProductDescription.Location = new System.Drawing.Point(13, 59);
            this.lbl_ProductDescription.Name = "lbl_ProductDescription";
            this.lbl_ProductDescription.Size = new System.Drawing.Size(60, 13);
            this.lbl_ProductDescription.TabIndex = 7;
            this.lbl_ProductDescription.Text = "Description";
            // 
            // txtbx_ContactName
            // 
            this.txtbx_ContactName.Location = new System.Drawing.Point(126, 34);
            this.txtbx_ContactName.Name = "txtbx_ContactName";
            this.txtbx_ContactName.Size = new System.Drawing.Size(257, 20);
            this.txtbx_ContactName.TabIndex = 6;
            // 
            // lbl_ProductName
            // 
            this.lbl_ProductName.AutoSize = true;
            this.lbl_ProductName.Location = new System.Drawing.Point(13, 37);
            this.lbl_ProductName.Name = "lbl_ProductName";
            this.lbl_ProductName.Size = new System.Drawing.Size(75, 13);
            this.lbl_ProductName.TabIndex = 5;
            this.lbl_ProductName.Text = "Contact Name";
            // 
            // combobx_CustomerType
            // 
            this.combobx_CustomerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combobx_CustomerType.FormattingEnabled = true;
            this.combobx_CustomerType.Location = new System.Drawing.Point(126, 7);
            this.combobx_CustomerType.Name = "combobx_CustomerType";
            this.combobx_CustomerType.Size = new System.Drawing.Size(257, 21);
            this.combobx_CustomerType.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Contact Type";
            // 
            // txtbx_ContactCity
            // 
            this.txtbx_ContactCity.Location = new System.Drawing.Point(126, 100);
            this.txtbx_ContactCity.Name = "txtbx_ContactCity";
            this.txtbx_ContactCity.Size = new System.Drawing.Size(257, 20);
            this.txtbx_ContactCity.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "City";
            // 
            // txtbx_ContactAddress
            // 
            this.txtbx_ContactAddress.Location = new System.Drawing.Point(126, 78);
            this.txtbx_ContactAddress.Name = "txtbx_ContactAddress";
            this.txtbx_ContactAddress.Size = new System.Drawing.Size(257, 20);
            this.txtbx_ContactAddress.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Address";
            // 
            // txtbx_ContactZipCode
            // 
            this.txtbx_ContactZipCode.Location = new System.Drawing.Point(308, 122);
            this.txtbx_ContactZipCode.Name = "txtbx_ContactZipCode";
            this.txtbx_ContactZipCode.Size = new System.Drawing.Size(75, 20);
            this.txtbx_ContactZipCode.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(256, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Zip Code";
            // 
            // txtbx_ContactState
            // 
            this.txtbx_ContactState.Location = new System.Drawing.Point(126, 122);
            this.txtbx_ContactState.Name = "txtbx_ContactState";
            this.txtbx_ContactState.Size = new System.Drawing.Size(105, 20);
            this.txtbx_ContactState.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "State";
            // 
            // txtbx_ContactPhoneNumber
            // 
            this.txtbx_ContactPhoneNumber.Location = new System.Drawing.Point(126, 159);
            this.txtbx_ContactPhoneNumber.Name = "txtbx_ContactPhoneNumber";
            this.txtbx_ContactPhoneNumber.Size = new System.Drawing.Size(257, 20);
            this.txtbx_ContactPhoneNumber.TabIndex = 21;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Phone Number";
            // 
            // txtbx_ContactEmailAddress
            // 
            this.txtbx_ContactEmailAddress.Location = new System.Drawing.Point(126, 181);
            this.txtbx_ContactEmailAddress.Name = "txtbx_ContactEmailAddress";
            this.txtbx_ContactEmailAddress.Size = new System.Drawing.Size(257, 20);
            this.txtbx_ContactEmailAddress.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 184);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Email Address";
            // 
            // txtbx_ContactNotes
            // 
            this.txtbx_ContactNotes.Location = new System.Drawing.Point(126, 207);
            this.txtbx_ContactNotes.Multiline = true;
            this.txtbx_ContactNotes.Name = "txtbx_ContactNotes";
            this.txtbx_ContactNotes.Size = new System.Drawing.Size(257, 68);
            this.txtbx_ContactNotes.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 210);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Notes";
            // 
            // EditContacts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 337);
            this.Controls.Add(this.txtbx_ContactNotes);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtbx_ContactEmailAddress);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtbx_ContactPhoneNumber);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtbx_ContactZipCode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtbx_ContactState);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtbx_ContactCity);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtbx_ContactAddress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.combobx_CustomerType);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.txtbx_ContactDescription);
            this.Controls.Add(this.lbl_ProductDescription);
            this.Controls.Add(this.txtbx_ContactName);
            this.Controls.Add(this.lbl_ProductName);
            this.Name = "EditContacts";
            this.Text = "Contacts";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void SaveContact(object sender, EventArgs e)
        {
            CustomerActions findContactType = new CustomerActions();
            
            Contact newContact = new Contact()
                                     {
                                         ContactTypeID = findContactType.GetContactType(combobx_CustomerType.SelectedItem.ToString()),
                                         Name = txtbx_ContactName.Text,
                                         Description = txtbx_ContactDescription.Text,
                                         Address = txtbx_ContactAddress.Text,
                                         City = txtbx_ContactCity.Text,
                                         State = txtbx_ContactState.Text,
                                         ZipCode = txtbx_ContactZipCode.Text,
                                         Phone = txtbx_ContactPhoneNumber.Text,
                                         EmailAddress = txtbx_ContactEmailAddress.Text,
                                         Notes = txtbx_ContactNotes.Text,
                                     };
       
            nVentoryDB.Contacts.Add(newContact);
            nVentoryDB.SaveChanges();

            Application.Restart();
        }
    }
}
