﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace nVentory
{
    public partial class Reporter : Form
    {
        nVentoryEntities nVentoryDB = new nVentoryEntities();

        public Reporter()
        {
            InitializeComponent();
        }

        private void Reporter_Load(object sender, EventArgs e)
        {
            // Set the data to show in the DataGridView.  [Data GridView was created using the control from the DataSources (after creating the link to the data type [from the Object])
            reportInv_ResultDataGridView.DataSource = nVentoryDB.ReportInv();
        }
    }
}
