﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using nVentory.Actions;

namespace nVentory
{
    public partial class EditProducts : Form
    {
        nVentoryEntities nVentoryDB = new nVentoryEntities();

        public EditProducts()
        {
            InitializeComponent();
        }

        private void SaveProduct(object sender, EventArgs e)
        {
            var addProd = new ProductActions();
            addProd.AddProduct(new Product()
            {
                Name = txtbx_ProductName.Text,
                PricePerItem = txtbx_ProductDescription.Text,
            });

            // Add a blank 0 instance for the item and its inventory, and set to 0 (until the user adds inventory to it for later)
            var clientSoldToOrBoughtFrom = new CustomerActions();
            var addNewInventory = new InventoryAction();
            addNewInventory.AddNewProductToInventory(addProd.GetProductId(txtbx_ProductName.Text));
            
            Application.Restart();
        }
    }
}
