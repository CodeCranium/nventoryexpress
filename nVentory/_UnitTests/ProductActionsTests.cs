﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using NUnit.Framework;
using nVentory.Actions;

namespace nVentory._UnitTests
{
    internal class ProductActionsTests
    {
        public ProductActions xProd = new ProductActions();

        [Test]
        public void ShouldSaveProduct()
        {
            var testProduct = new Product() { Name = "Test Product", PricePerItem = "22.50" };
            xProd.AddProduct(testProduct);
        }

        [Test]
        public void ShouldShowPriceInCurrencyFormat()
        {
            foreach (var productList in xProd.GetAllProducts())
            {
                Debug.WriteLine(productList.PricePerItem);
            }
            
        }
    }
}
