﻿namespace nVentory
{
    partial class EditProducts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_ProductName = new System.Windows.Forms.Label();
            this.txtbx_ProductName = new System.Windows.Forms.TextBox();
            this.txtbx_ProductDescription = new System.Windows.Forms.TextBox();
            this.lbl_ProductDescription = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_ProductName
            // 
            this.lbl_ProductName.AutoSize = true;
            this.lbl_ProductName.Location = new System.Drawing.Point(12, 15);
            this.lbl_ProductName.Name = "lbl_ProductName";
            this.lbl_ProductName.Size = new System.Drawing.Size(75, 13);
            this.lbl_ProductName.TabIndex = 0;
            this.lbl_ProductName.Text = "Product Name";
            // 
            // txtbx_ProductName
            // 
            this.txtbx_ProductName.Location = new System.Drawing.Point(125, 12);
            this.txtbx_ProductName.Name = "txtbx_ProductName";
            this.txtbx_ProductName.Size = new System.Drawing.Size(257, 20);
            this.txtbx_ProductName.TabIndex = 1;
            // 
            // txtbx_ProductDescription
            // 
            this.txtbx_ProductDescription.Location = new System.Drawing.Point(125, 38);
            this.txtbx_ProductDescription.Name = "txtbx_ProductDescription";
            this.txtbx_ProductDescription.Size = new System.Drawing.Size(257, 20);
            this.txtbx_ProductDescription.TabIndex = 3;
            // 
            // lbl_ProductDescription
            // 
            this.lbl_ProductDescription.AutoSize = true;
            this.lbl_ProductDescription.Location = new System.Drawing.Point(12, 41);
            this.lbl_ProductDescription.Name = "lbl_ProductDescription";
            this.lbl_ProductDescription.Size = new System.Drawing.Size(100, 13);
            this.lbl_ProductDescription.TabIndex = 2;
            this.lbl_ProductDescription.Text = "Product Description";
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(307, 74);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(75, 23);
            this.btn_Save.TabIndex = 4;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.SaveProduct);
            // 
            // editProducts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 133);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.txtbx_ProductDescription);
            this.Controls.Add(this.lbl_ProductDescription);
            this.Controls.Add(this.txtbx_ProductName);
            this.Controls.Add(this.lbl_ProductName);
            this.Name = "editProducts";
            this.Text = "Products";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_ProductName;
        private System.Windows.Forms.TextBox txtbx_ProductName;
        private System.Windows.Forms.TextBox txtbx_ProductDescription;
        private System.Windows.Forms.Label lbl_ProductDescription;
        private System.Windows.Forms.Button btn_Save;
    }
}