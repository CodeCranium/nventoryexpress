﻿namespace nVentory
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLink_AddProduct = new System.Windows.Forms.LinkLabel();
            this.btnLink_RemoveProduct = new System.Windows.Forms.LinkLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage_Products = new System.Windows.Forms.TabPage();
            this.combobx_ProductItems = new System.Windows.Forms.ComboBox();
            this.tabPage_Contacts = new System.Windows.Forms.TabPage();
            this.combobx_Contacts = new System.Windows.Forms.ComboBox();
            this.btnLink_AddCustomer = new System.Windows.Forms.LinkLabel();
            this.btnLink_RemoveCustomer = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.numUpDown_RemoveProductAmount = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.combobx_Products = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtbx_CurrentInventoryDisplay = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.numUpDown_AddProductAmount = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.combobx_Clients = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.tabPage_Products.SuspendLayout();
            this.tabPage_Contacts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDown_RemoveProductAmount)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDown_AddProductAmount)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLink_AddProduct
            // 
            this.btnLink_AddProduct.AutoSize = true;
            this.btnLink_AddProduct.Location = new System.Drawing.Point(8, 30);
            this.btnLink_AddProduct.Name = "btnLink_AddProduct";
            this.btnLink_AddProduct.Size = new System.Drawing.Size(66, 13);
            this.btnLink_AddProduct.TabIndex = 1;
            this.btnLink_AddProduct.TabStop = true;
            this.btnLink_AddProduct.Text = "Add Product";
            this.btnLink_AddProduct.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.AddProduct);
            // 
            // btnLink_RemoveProduct
            // 
            this.btnLink_RemoveProduct.AutoSize = true;
            this.btnLink_RemoveProduct.Location = new System.Drawing.Point(80, 30);
            this.btnLink_RemoveProduct.Name = "btnLink_RemoveProduct";
            this.btnLink_RemoveProduct.Size = new System.Drawing.Size(87, 13);
            this.btnLink_RemoveProduct.TabIndex = 2;
            this.btnLink_RemoveProduct.TabStop = true;
            this.btnLink_RemoveProduct.Text = "Remove Product";
            this.btnLink_RemoveProduct.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.RemoveProduct);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage_Products);
            this.tabControl1.Controls.Add(this.tabPage_Contacts);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(0, 175);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(432, 77);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage_Products
            // 
            this.tabPage_Products.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage_Products.Controls.Add(this.combobx_ProductItems);
            this.tabPage_Products.Controls.Add(this.btnLink_AddProduct);
            this.tabPage_Products.Controls.Add(this.btnLink_RemoveProduct);
            this.tabPage_Products.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Products.Name = "tabPage_Products";
            this.tabPage_Products.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Products.Size = new System.Drawing.Size(424, 51);
            this.tabPage_Products.TabIndex = 0;
            this.tabPage_Products.Text = "Products";
            // 
            // combobx_ProductItems
            // 
            this.combobx_ProductItems.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.combobx_ProductItems.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combobx_ProductItems.FormattingEnabled = true;
            this.combobx_ProductItems.Location = new System.Drawing.Point(6, 6);
            this.combobx_ProductItems.Name = "combobx_ProductItems";
            this.combobx_ProductItems.Size = new System.Drawing.Size(410, 21);
            this.combobx_ProductItems.TabIndex = 3;
            // 
            // tabPage_Contacts
            // 
            this.tabPage_Contacts.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage_Contacts.Controls.Add(this.combobx_Contacts);
            this.tabPage_Contacts.Controls.Add(this.btnLink_AddCustomer);
            this.tabPage_Contacts.Controls.Add(this.btnLink_RemoveCustomer);
            this.tabPage_Contacts.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Contacts.Name = "tabPage_Contacts";
            this.tabPage_Contacts.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Contacts.Size = new System.Drawing.Size(424, 51);
            this.tabPage_Contacts.TabIndex = 1;
            this.tabPage_Contacts.Text = "Contacts";
            // 
            // combobx_Contacts
            // 
            this.combobx_Contacts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.combobx_Contacts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combobx_Contacts.FormattingEnabled = true;
            this.combobx_Contacts.Location = new System.Drawing.Point(6, 7);
            this.combobx_Contacts.Name = "combobx_Contacts";
            this.combobx_Contacts.Size = new System.Drawing.Size(387, 21);
            this.combobx_Contacts.TabIndex = 6;
            // 
            // btnLink_AddCustomer
            // 
            this.btnLink_AddCustomer.AutoSize = true;
            this.btnLink_AddCustomer.Location = new System.Drawing.Point(8, 31);
            this.btnLink_AddCustomer.Name = "btnLink_AddCustomer";
            this.btnLink_AddCustomer.Size = new System.Drawing.Size(73, 13);
            this.btnLink_AddCustomer.TabIndex = 4;
            this.btnLink_AddCustomer.TabStop = true;
            this.btnLink_AddCustomer.Text = "Add Customer";
            this.btnLink_AddCustomer.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.AddCustomer);
            // 
            // btnLink_RemoveCustomer
            // 
            this.btnLink_RemoveCustomer.AutoSize = true;
            this.btnLink_RemoveCustomer.Location = new System.Drawing.Point(87, 31);
            this.btnLink_RemoveCustomer.Name = "btnLink_RemoveCustomer";
            this.btnLink_RemoveCustomer.Size = new System.Drawing.Size(94, 13);
            this.btnLink_RemoveCustomer.TabIndex = 5;
            this.btnLink_RemoveCustomer.TabStop = true;
            this.btnLink_RemoveCustomer.Text = "Remove Customer";
            this.btnLink_RemoveCustomer.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.RemoveContact);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.linkLabel1.Location = new System.Drawing.Point(231, 141);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(181, 13);
            this.linkLabel1.TabIndex = 4;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "+ Add to Inventory/Stock [Purchase]";
            this.linkLabel1.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.AddToInventory);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.LinkColor = System.Drawing.Color.Red;
            this.linkLabel2.Location = new System.Drawing.Point(32, 139);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(182, 13);
            this.linkLabel2.TabIndex = 5;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "- Remove from Inventory/Stock [Sell]";
            this.linkLabel2.VisitedLinkColor = System.Drawing.Color.Red;
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.RemoveFromInventory);
            // 
            // numUpDown_RemoveProductAmount
            // 
            this.numUpDown_RemoveProductAmount.Location = new System.Drawing.Point(97, 116);
            this.numUpDown_RemoveProductAmount.Name = "numUpDown_RemoveProductAmount";
            this.numUpDown_RemoveProductAmount.Size = new System.Drawing.Size(104, 20);
            this.numUpDown_RemoveProductAmount.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Amount:";
            // 
            // combobx_Products
            // 
            this.combobx_Products.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.combobx_Products.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combobx_Products.FormattingEnabled = true;
            this.combobx_Products.Location = new System.Drawing.Point(69, 37);
            this.combobx_Products.Name = "combobx_Products";
            this.combobx_Products.Size = new System.Drawing.Size(354, 21);
            this.combobx_Products.TabIndex = 8;
            this.combobx_Products.SelectedIndexChanged += new System.EventHandler(this.ChangeOfProduct);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Product:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtbx_CurrentInventoryDisplay);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.numUpDown_AddProductAmount);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.combobx_Clients);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.linkLabel1);
            this.panel1.Controls.Add(this.combobx_Products);
            this.panel1.Controls.Add(this.linkLabel2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.numUpDown_RemoveProductAmount);
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(432, 165);
            this.panel1.TabIndex = 10;
            // 
            // txtbx_CurrentInventoryDisplay
            // 
            this.txtbx_CurrentInventoryDisplay.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtbx_CurrentInventoryDisplay.Location = new System.Drawing.Point(69, 87);
            this.txtbx_CurrentInventoryDisplay.Name = "txtbx_CurrentInventoryDisplay";
            this.txtbx_CurrentInventoryDisplay.ReadOnly = true;
            this.txtbx_CurrentInventoryDisplay.Size = new System.Drawing.Size(354, 13);
            this.txtbx_CurrentInventoryDisplay.TabIndex = 14;
            this.txtbx_CurrentInventoryDisplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(243, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Amount:";
            // 
            // numUpDown_AddProductAmount
            // 
            this.numUpDown_AddProductAmount.Location = new System.Drawing.Point(295, 118);
            this.numUpDown_AddProductAmount.Maximum = new decimal(new int[] {
            276447231,
            23283,
            0,
            0});
            this.numUpDown_AddProductAmount.Name = "numUpDown_AddProductAmount";
            this.numUpDown_AddProductAmount.Size = new System.Drawing.Size(104, 20);
            this.numUpDown_AddProductAmount.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Client:";
            // 
            // combobx_Clients
            // 
            this.combobx_Clients.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.combobx_Clients.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combobx_Clients.FormattingEnabled = true;
            this.combobx_Clients.Location = new System.Drawing.Point(69, 60);
            this.combobx_Clients.Name = "combobx_Clients";
            this.combobx_Clients.Size = new System.Drawing.Size(354, 21);
            this.combobx_Clients.TabIndex = 10;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(432, 24);
            this.menuStrip1.TabIndex = 15;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reportsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.reportsToolStripMenuItem.Text = "Reports";
            this.reportsToolStripMenuItem.Click += new System.EventHandler(this.ShowReports);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 252);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "nVentory Express";
            this.tabControl1.ResumeLayout(false);
            this.tabPage_Products.ResumeLayout(false);
            this.tabPage_Products.PerformLayout();
            this.tabPage_Contacts.ResumeLayout(false);
            this.tabPage_Contacts.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDown_RemoveProductAmount)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDown_AddProductAmount)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.LinkLabel btnLink_AddProduct;
        private System.Windows.Forms.LinkLabel btnLink_RemoveProduct;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage_Products;
        private System.Windows.Forms.TabPage tabPage_Contacts;
        private System.Windows.Forms.ComboBox combobx_ProductItems;
        private System.Windows.Forms.ComboBox combobx_Contacts;
        private System.Windows.Forms.LinkLabel btnLink_AddCustomer;
        private System.Windows.Forms.LinkLabel btnLink_RemoveCustomer;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.NumericUpDown numUpDown_RemoveProductAmount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox combobx_Products;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox combobx_Clients;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numUpDown_AddProductAmount;
        private System.Windows.Forms.TextBox txtbx_CurrentInventoryDisplay;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        // internal static System.Windows.Forms.ComboBox combobx_ProductItems;
    }
}

