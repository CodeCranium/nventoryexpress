﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nVentory.Actions
{
    class _DeleteDbItem
    {
        private nVentoryEntities nVentoryDB = new nVentoryEntities();

        // Generic Remove 
        public void Remove<T>(string nameOfItemToRemove)
        {
            if (typeof(T).Name == "Product")
            {
                nVentoryDB.Products.Remove(nVentoryDB.Products.FirstOrDefault(x => x.Name == nameOfItemToRemove));
                nVentoryDB.SaveChanges();
            }
            else if (typeof(T).Name == "Contact")
            {
                nVentoryDB.Contacts.Remove(nVentoryDB.Contacts.FirstOrDefault(x => x.Name == nameOfItemToRemove));
                nVentoryDB.SaveChanges();
            }
        }
    }
}
