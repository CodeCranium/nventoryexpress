﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace nVentory.Actions
{
    class LoadStartupData : Form1
    {
        public static nVentoryEntities nVentoryDB = new nVentoryEntities();

        internal static void LoadProductsDropDown(ref ComboBox dropdownItemProducts)
        {
            foreach (var prodItem in nVentoryDB.Products)
            {
                dropdownItemProducts.Items.Add(prodItem.Name);
            }

            if (dropdownItemProducts.Items.Count >= 1)
            {
                dropdownItemProducts.SelectedItem = dropdownItemProducts.Items[0];
            }

            // combobx_ProductItems.Items.Add(nVentoryDB.Products.Where(x=>x.Name != null));

        }

        internal static void LoadCustomersDropDown(ref ComboBox dropdownItemContacts)
        {
            foreach (var contactItem in nVentoryDB.Contacts)
            {
                dropdownItemContacts.Items.Add(contactItem.Name);
            }

            if (dropdownItemContacts.Items.Count >= 1)
            {
                dropdownItemContacts.SelectedItem = dropdownItemContacts.Items[0];
            }
        }
    }
}
