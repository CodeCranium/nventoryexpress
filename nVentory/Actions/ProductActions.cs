﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace nVentory.Actions
{
    internal class ProductActions
    {
        public nVentoryEntities nVentoryDB = new nVentoryEntities();

        internal void AddProduct(nVentory.Product productInfo)
        {
            if (productInfo != null) nVentoryDB.Products.Add(entity: productInfo);
            nVentoryDB.SaveChanges();
        }

        public List<nVentory.Product> GetAllProducts()
        {
            return nVentoryDB.Products.ToList();
        }

        public void RemoveProduct<T>(string productNameToRemove)
        {
            //Remove any inventory instances, (if the user is removing the product then the instance of its inventory will need to be removed also)
            int productID = GetProductId(productNameToRemove);
            var removeProductFromInventory = new InventoryAction();
            removeProductFromInventory.ClearProduct(productName: productNameToRemove);

            //Remove the product from the Products Table (needs to be done after removing the Inventory References, so it dosn't cause Null Reference)
            _DeleteDbItem removeProduct = new _DeleteDbItem();
            removeProduct.Remove<nVentory.Product>(productNameToRemove);

            nVentoryDB.SaveChanges();
        }

        public int GetProductId(string productName)
        {
            try
            {
                var product = nVentoryDB.Products.FirstOrDefault(x => x.Name == productName);
                if (product != null)
                {
                    return product.ProductID;
                }
                throw new NullReferenceException("No product with the specified information was found in the database.");
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }
    }
}
