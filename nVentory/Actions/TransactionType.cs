﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nVentory.Actions
{
    public class TransactionType
    {
        public enum TransType
        {
            NA = 0,
            AddInventory = 1,
            SoldInventory = 2,
            NewItemAddedToProducts = 3,
            RemovedProduct = 4,
        }
    }
}
