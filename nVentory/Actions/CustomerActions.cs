﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nVentory.Actions
{
    class CustomerActions
    {
        private nVentoryEntities nVentoryDB = new nVentoryEntities();

        public void Remove<T>(string customerNameToRemove)
        {
            _DeleteDbItem removeCustomer = new _DeleteDbItem();
            removeCustomer.Remove<nVentory.Contact>(customerNameToRemove);
            nVentoryDB.SaveChanges();
        }

        public int GetContactType(string contactTypeName)
        {
            var contactType = nVentoryDB.ContactTypes.FirstOrDefault(x => x.TypeName == contactTypeName);
            if (contactType != null)
            {
                return contactType.ContactTypeID;
            }
            throw new NullReferenceException(String.Format("No matching Customer Type for: ", contactTypeName));
            //(int)Enum.Parse(typeof(TransactionType.TransType), contactType);
        }
    }
}
