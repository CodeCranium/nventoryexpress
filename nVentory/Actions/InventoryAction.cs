﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nVentory.Actions
{
    class InventoryAction
    {
        nVentoryEntities nVentoryDB = new nVentoryEntities();
        
        public void AddInventory(string selectedProduct, string purchasedFrom, int numberToAdd)
        {
            var prodInv = SelectInventoryInfo(selectedProduct);
            prodInv.NumberOfItemsInCurrentInventory += numberToAdd;
            nVentoryDB.SaveChanges();

            RecordToLog(prodInv.ProductID, GetCustomerID(purchasedFrom), numberToAdd, TransactionType.TransType.AddInventory);
        }

        private void RecordToLog(int productId, int customerId, int quantity, TransactionType.TransType transType)
        {
            nVentoryDB.Operations.Add(new Operation()
                                          {
                                              ContactID = customerId,
                                              ContactTypeID = GetCustomerTypeID(customerId),
                                              ProductID = productId,
                                              Quantity = quantity,
                                              DateOfOperation = DateTime.Now,
                                              TransactionTypeID = (int) transType
                                          });
            nVentoryDB.SaveChanges();
        }

        private void RecordToLog(string productName, int customerId, int quantity, TransactionType.TransType transType)
        {
            ProductActions getProductID = new ProductActions();

            RecordToLog(getProductID.GetProductId(productName: productName), customerId: customerId, quantity: quantity, transType: transType);
        }


        private int GetCustomerID(string customerName)
        {
            var customer = nVentoryDB.Contacts.FirstOrDefault(x => x.Name == customerName);
            if (customer != null)
            {
                return customer.ContactID;
            }
            return 0;
        }
        private int GetCustomerTypeID(int customerId)
        {
            var customer = nVentoryDB.Contacts.FirstOrDefault(x => x.ContactID == customerId);
            if (customer != null)
            {
                return customer.ContactTypeID;
            }
            return 0;
        }


        public int GetCurrentNumberInInventory(string nameOfProduct)
        {
            return SelectInventoryInfo(nameOfProduct).NumberOfItemsInCurrentInventory;
        }

        public Inventory SelectInventoryInfo(string nameOfProduct)
        {
            var productSearch = nVentoryDB.Products.FirstOrDefault(x => x.Name == nameOfProduct);
            if (productSearch != null)
            {
                // Retrieve ProductID to use with 
                var getProductID = productSearch.ProductID;
                var getInventoryItem = nVentoryDB.Inventories.FirstOrDefault(x => x.ProductID == getProductID);
                if (getInventoryItem != null)
                {
                    return getInventoryItem;
                }
                throw new NullReferenceException();
            }
            throw new NullReferenceException();
        }

        public void RemoveInventory(string selectedProduct, string soldToCustomer, int numberToRemove)
        {
            var prodInv = SelectInventoryInfo(selectedProduct);
            prodInv.NumberOfItemsInCurrentInventory -= numberToRemove;
            nVentoryDB.SaveChanges();

            RecordToLog(prodInv.ProductID, GetCustomerID(soldToCustomer), numberToRemove, TransactionType.TransType.SoldInventory);
        }

        public void AddNewProductToInventory(int productId)
        {
            nVentoryDB.Inventories.Add(new Inventory()
                                           {
                                               NumberOfItemsInCurrentInventory = 0,
                                               ProductID = productId
                                           });
            nVentoryDB.SaveChanges();
            RecordToLog(productId, 0, 0, TransactionType.TransType.NewItemAddedToProducts);
        }

        public void ClearProduct(string productName)
        {
            nVentoryDB.Inventories.Remove(SelectInventoryInfo(productName));
            nVentoryDB.SaveChanges();
            RecordToLog(productName, 0, 0, TransactionType.TransType.RemovedProduct);
        }
    }
}
