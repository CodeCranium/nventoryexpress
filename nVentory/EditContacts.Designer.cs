﻿namespace nVentory
{
    partial class EditContacts
    {

        #region Windows Form Designer generated code


        #endregion

        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.TextBox txtbx_ContactDescription;
        private System.Windows.Forms.Label lbl_ProductDescription;
        private System.Windows.Forms.TextBox txtbx_ContactName;
        private System.Windows.Forms.Label lbl_ProductName;
        private System.Windows.Forms.ComboBox combobx_CustomerType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtbx_ContactCity;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtbx_ContactAddress;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtbx_ContactZipCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtbx_ContactState;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtbx_ContactPhoneNumber;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtbx_ContactEmailAddress;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtbx_ContactNotes;
        private System.Windows.Forms.Label label8;
    }
}