﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using nVentory.Actions;
using nVentory.DB;

namespace nVentory
{
    public partial class Form1 : Form
    {
        nVentoryEntities nVentoryDB = new nVentoryEntities();
        public Form1()
        {
            //Check to ensure that Datbase connection (if no database connection, prompt user to create one or correct the connection)
            try
            {
                InitializeComponent();
                LoadStartupData.LoadProductsDropDown(ref combobx_ProductItems);
                LoadStartupData.LoadProductsDropDown(ref combobx_Products);

                LoadStartupData.LoadCustomersDropDown(ref combobx_Contacts);
                LoadStartupData.LoadCustomersDropDown(ref combobx_Clients);
            }
            catch (EntityException DatabaseError)
            {
                DB.DatabaseErrorHandling databaseException = new DatabaseErrorHandling();
                databaseException.StartupErrorsDB(DatabaseError);
            }
        }



        private void SaveInfo(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void AddProduct(object sender, LinkLabelLinkClickedEventArgs e)
        {
            EditProducts AddProduct = new EditProducts();
            AddProduct.Show();
        }

        private void RemoveProduct(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ProductActions RemoveProduct = new ProductActions();
            RemoveProduct.RemoveProduct<Product>(combobx_ProductItems.SelectedItem.ToString());
            Application.Restart();
        }

        private void AddCustomer(object sender, LinkLabelLinkClickedEventArgs e)
        {
            EditContacts AddContact = new EditContacts();
            AddContact.Show();
        }

        private void RemoveContact(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CustomerActions RemoveCustomer = new CustomerActions();
            RemoveCustomer.Remove<Contact>(combobx_Contacts.SelectedItem.ToString());
            Application.Restart();
        }

        private void ChangeOfProduct(object sender, EventArgs e)
        {
            var getInventory = new InventoryAction();
            int numberOfInventory = getInventory.GetCurrentNumberInInventory(combobx_Products.SelectedItem.ToString());
            numUpDown_RemoveProductAmount.Maximum = numberOfInventory;
            txtbx_CurrentInventoryDisplay.Text = String.Format("Number in current Inventory: {0}", numberOfInventory);
        }

        private void AddToInventory(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                if (! int.Parse(numUpDown_AddProductAmount.Text).IsGreaterThanZero())
                {
                    throw new InvalidDataException("Selected amount is not valid.");
                }

                var addInventory = new InventoryAction();
                addInventory.AddInventory(combobx_Products.SelectedItem.ToString(),
                                          combobx_Clients.SelectedItem.ToString(),
                                          int.Parse(numUpDown_AddProductAmount.Text));

                // Update the number of items to available, so they can be removed if needed. 
                ChangeOfProduct(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RemoveFromInventory(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                if (!int.Parse(numUpDown_RemoveProductAmount.Text).IsGreaterThanZero())
                {
                    throw new InvalidDataException("Selected amount is not valid.");
                }

                var removeInventory = new InventoryAction();
                removeInventory.RemoveInventory(combobx_Products.SelectedItem.ToString(),
                                                combobx_Clients.SelectedItem.ToString(),
                                                int.Parse(numUpDown_RemoveProductAmount.Text));

                ChangeOfProduct(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ShowReports(object sender, EventArgs e)
        {
            Reporter x1 = new Reporter();
            x1.Show();
        }
    }
}
